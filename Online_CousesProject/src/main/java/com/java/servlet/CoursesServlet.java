package com.java.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.java.connection.DbConnection;

import com.java.dao.CoursesDao;
import com.java.model.Courses;
import com.java.model.User;



@WebServlet("/CoursesServlet")
public class CoursesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		response.setContentType("text/html;charset-UTF-8");
		try (PrintWriter out = response.getWriter()){
			
			int uid;
			User auth = (User) request.getSession().getAttribute("auth");
			
				CoursesDao coursesDao = new CoursesDao(DbConnection.getConnection());
						
				if ( auth != null) {
					if(!(session.getAttribute("userId")==null)) {
						String userId = (String)session.getAttribute("userId");
						uid=Integer.parseInt(userId);
						System.out.println("UserId customer" +userId);	
						coursesDao.uid=uid;
						
				String fsname = request.getParameter("fsname");
				String lsname = request.getParameter("lsname");
                String courses = request.getParameter("course");
                String startDate =request.getParameter("startdate");
                String endDate =request.getParameter("enddate");
                
                Courses courselanguage = new Courses();
                courselanguage.setFname(fsname);
                courselanguage.setLname(lsname);
                courselanguage.setCourses(courses);
                courselanguage.setStartdate(startDate);
                courselanguage.setEnddate(endDate);
                courselanguage.setUid(auth.getId());
          
                
                System.out.println(fsname+lsname+courses+ endDate +startDate);
            	
            	
            	Courses courses1 = coursesDao.coursesInsert(courselanguage);
            	System.out.println(courselanguage);
        		System.out.println(courses1);
    			if(!courselanguage.equals(null)) {
    				
    				response.sendRedirect("view.jsp");
    				out.print("user login");
    				System.out.println("Transfering");
    			   }
            	
				}

			 
			}else {
				if (auth == null) {
					response.sendRedirect("login.jsp");
				}
          }
	}catch (Exception e) {
        e.printStackTrace();
    }
}
}