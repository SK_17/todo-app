package com.java.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.GeneralSecurityException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.java.connection.DbConnection;
import com.java.dao.UserDao;
import com.java.model.User;


@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset-UTF-8");
		
		
		
		try (PrintWriter out = response.getWriter()){
			String Email = request.getParameter("email");
			String Password = request.getParameter("password");
			
			try {
			UserDao udao = new UserDao(DbConnection.getConnection());
			
				
			User user = udao.userLogin(Email,Password);
			if(user !=null) {
				request.getSession().setAttribute("auth", user);
				int id = user.getId();
				String userId = Integer.toString(id);
				HttpSession session = request.getSession();
				session.setAttribute("userId", userId);
				response.sendRedirect("index.jsp");
				out.print("user login");
				System.out.println("Transfering");
				
			}else {
				
				System.out.println("Failed");
				out.println("Invalid Email or Password");
			}
			
		    } catch (ClassNotFoundException | SQLException | GeneralSecurityException e) {

			e.printStackTrace();
		}
	}
 }
}

