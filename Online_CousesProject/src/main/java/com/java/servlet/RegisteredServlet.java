package com.java.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.java.connection.DbConnection;
import com.java.dao.RegisteredDao;
import com.java.model.Registered;


@WebServlet("/RegisteredServlet")
public class RegisteredServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
   

	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		response.setContentType("text/html;charset-UTF-8");
		try(PrintWriter out = response.getWriter()){
			
			String First_Name=request.getParameter("fname");
			String Last_Name=request.getParameter("lname");
			String Email=request.getParameter("email");
			String Password=request.getParameter("password");
			Registered registered= new Registered();
			RequestDispatcher dispatcher =null;

				RegisteredDao rdao= new RegisteredDao(DbConnection.getConnection());
				System.out.println(First_Name+Email+ Last_Name+ Password);
				
			    registered = rdao.registeredSignUp(First_Name, Last_Name, Email, Password);

			    response.sendRedirect("login.jsp");
//			    System.out.println(registered);
//                   if(registered != null) {
//                	   RequestDispatcher dispatcher1 = request.getRequestDispatcher("login.jsp");
//    				response.sendRedirect("login.jsp");
//    				out.print("user login");
//    				System.out.println("Transfering");
//                   }
//			   else {
//			
//					System.out.println("Failed");
//				  
//                }
	          }catch (Exception e) {
               e.printStackTrace();
           }
			
		
	}

}