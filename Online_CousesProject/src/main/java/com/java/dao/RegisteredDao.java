package com.java.dao;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import com.java.model.Registered;


public class RegisteredDao {
	private Connection con;
	private String query;
	private PreparedStatement pst;
	private ResultSet rs;
	
	public RegisteredDao(Connection con) {
		this.con=con;
	}
	
	public  Registered registeredSignUp(String fname,String lname,String email, String password) 
			throws GeneralSecurityException {
		
		Registered registered =null;
		
		try {
			query = "insert into registered (fname,lname,email,password) values (?,?,?,?)";
			pst = this.con.prepareStatement(query);
			pst.setString(1, fname);
			pst.setString(2, lname);
			pst.setString(3, email);
			pst.setString(4, MD5(password));
			int rowcount =pst.executeUpdate();
			
			
			
			if(rowcount>0) {
				System.out.print("registered successfully");
				
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
			
		}
		return registered;
	
		
	}

	private String MD5(String s)throws GeneralSecurityException {
			 MessageDigest m=MessageDigest.getInstance("MD5");
			 m.update(s.getBytes(),0,s.length());
             return new BigInteger(1,m.digest()).toString(16);

	}
	
	
}
