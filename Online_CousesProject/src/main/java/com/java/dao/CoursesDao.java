package com.java.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;

import com.java.model.Courses;




public class CoursesDao {

	private Connection con;
	private String query;
	private PreparedStatement pst;
	private ResultSet rs;
	public static int uid;
	
	
	public CoursesDao(Connection con) {
		this.con = con;
	}


	public Courses coursesInsert(Courses courselang)  {
		
		 Courses courseslang=null;
		 
		try {
			
            query = "insert into course(fname,lname,course,startdate,enddate,uid) values(?,?,?,?,?,?)";
			
			pst = this.con.prepareStatement(query);
			
		
			pst.setString(1, courselang.getFname());
			pst.setString(2, courselang.getLname());
			pst.setString(3, courselang.getCourses());
			pst.setString(4,courselang.getStartdate());
			pst.setString(5, courselang.getEnddate());
			pst.setInt(6, uid);
			
			pst.executeUpdate();
	
		}catch (Exception e) {
			e.printStackTrace();
		}
		return courseslang;
	}
	public List<Courses> userCourses(int id){
		List<Courses> list = new ArrayList<>();
		try {
			
			query="select * from course where uid=?";
			pst = this.con.prepareStatement(query);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			
			while(rs.next()){
			
				Courses courses = new Courses();
				
				courses.setCourses(courses.getCourses());
				courses.setStartdate(courses.getStartdate());
				courses.setEnddate(courses.getEnddate());
			
				
				list.add(courses);
				System.out.println(courses);
				
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		return list;
		
	}

}
