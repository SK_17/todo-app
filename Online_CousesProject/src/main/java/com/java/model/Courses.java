package com.java.model;



public class Courses extends Registered{
	
	private int id;
	private String courses;
	private String startdate;
	private String enddate;
	private int uid;
	
	
	public Courses() {
		
	}


	public Courses(int id, String courses, String startdate, String enddate,int uid) {
		this.id = id;
		this.courses = courses;
		this.startdate = startdate;
		this.enddate = enddate;
		this.uid=uid;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getCourses() {
		return courses;
	}


	public void setCourses(String courses) {
		this.courses = courses;
	}


	public String getStartdate() {
		return startdate;
	}


	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}


	public String getEnddate() {
		return enddate;
	}


	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}


	public int getUid() {
		return uid;
	}


	public void setUid(int uid) {
		this.uid = uid;
	}


	@Override
	public String toString() {
		return "Courses [id=" + id + ", courses=" + courses + ", startdate=" + startdate + ", enddate=" + enddate
				+ ", uid=" + uid + "]";
	}


	
}
