<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.java.model.*"%>
<% 
      User auth =(User) request.getSession().getAttribute("auth");
      if(auth!=null){
	      request.setAttribute("person", auth);
       }
%>
       
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Welcome Page</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">


<link href="style/create.css" rel="stylesheet" type="text/css">
</head>
<body>
<%@include file = "includes/navbar.jsp" %>
<br>
<div class="d-grid gap-2 col-6 mx-auto">
<a class="btn btn-primary btn-lg" href="create.jsp" role="button">Create ToDos</a>
<a class="btn btn-warning btn-lg" href="view.jsp" role="button">Views ToDos</a>

</div>

<script type="text/javascript" src="js/bootstrap.js"></script>
<br>
<br>
<%@include file = "includes/footer.jsp" %>
</body>
</html>