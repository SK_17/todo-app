<%@page import="com.java.connection.DbConnection"%>
<%@page import="com.java.dao.CoursesDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page language="java" import="java.util.*" %>
<%@page import="com.java.model.*"%>
    
<% 
      User auth =(User) request.getSession().getAttribute("auth");
            if(auth!=null){
	      request.setAttribute("person", auth);
	     
	      
       }    
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">

<link href="style/create.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 

</head>
<body>
<%@include file ="includes/navbar.jsp" %>

<form name="login" action="CoursesServlet" method="post">
<div class="container register">
                <div class="row">
                    <div class="col-md-3 register-left">
                        <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
                        <h3>Welcome</h3>
                        <p>You are 30 seconds away from learning something new!</p>
                       
                    </div>
                    <div class="col-md-9 register-right">
                        <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Student</a>
                            </li>
                            
                        </ul>
         
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <h3 class="register-heading">Enroll for the courses</h3>
                                <div class="row register-form">
                                    <div class="col-md-6">
                                        <div class="form-group" align="center">
                                            <input type="text" class="form-control" placeholder="First Name *" value="" name="fsname" required/>
                                        </div><br>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Last Name *" value="" name="lsname" required/>
                                        </div><br>
                                        <div class="input-group-prepend">
                                            <div class="input-group mb-3">
                                            
                                               <div class="input-group-prepend" >
                                                   <label class="input-group-text" for="inputGroupSelect01">Courses</label>
                                                       </div>
                                                          
                                                    <%
                                                         ResourceBundle messages =ResourceBundle.getBundle("Courses");
                                                         Enumeration messageKeys = messages.getKeys();
                                                         %><select class="custom-select" id="inputGroupSelect01" class="col-xs-4" name="course">
                                                         <%
                                                          while(messageKeys.hasMoreElements()){
    	                                                   String key = (String)messageKeys.nextElement();
    	                                                   String value = messages.getString(key);
                                                         %>
                                                               <option selected >
                                                         <%=messages.getString(key)%>
                                                         </option>
                                                        <%
                                                        }
                                                     %>
                                                              
                                                          </select>
                                                </div>
                                         <label class="control-label col-sm-5 requiredField" for="date" >
                                           Start Date
                                              <span class="asteriskField">
                                                *
                                              </span>
                                         </label>
                                           <div class="col-sm-15">
                                            <div class="input-group">
                                               <div class="input-group-addon">
                                                  <i class="fa fa-calendar">
                                                       </i>
                                                              &nbsp;                                                 </div>
                                                            <input class="form-control" id="date" name="startdate" placeholder="MM/DD/YYYY" type="text"/>
                                                         </div><br>
                                     <label class="control-label col-sm-5 requiredField" for="date">
                                                    End Date
                                                       <span class="asteriskField">
                                                              *
                                                           </span>
                                                 </label>
                                                         <div class="col-sm-15">
                                                            <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                      <i class="fa fa-calendar">
                                                                             </i>
                                                              &nbsp; </div>
                                                            <input class="form-control" id="date" name="enddate" placeholder="MM/DD/YYYY" type="text"/>
                                                         </div>
                                                <div class="form-group">
                                            <div class="maxl">
                                               
                                            </div>
                                        </div>
                                     </div>
                                    <div class="col-md-6">
                                   
                                                </div>
                                                
                                                     </div>
                                                 <div class="form-group">                   
                                                  <input type="submit" class="btnRegister"  value="Enroll"/>
                                                 </div>
                                              </div>
                                          </div>
                                         
                                    </div>
                              </div>
                          </div>
                      </div>
                </div>
       
               </div>
              </form>
                <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
                
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>       <script>
	            $(document).ready(function(){
		        var date_input=$('input[name="startdate"]');
		        var date_input1=$('input[name="enddate"]');//our date input has the name "date"
		        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		        date_input.datepicker({
			    format: 'mm/dd/yyyy',
			    container: container,
			    todayHighlight: true,
			    autoclose: true,
		})
		date_input1.datepicker({
			    format: 'mm/dd/yyyy',
			    container: container,
			    todayHighlight: true,
			    autoclose: true,
		})
	})
</script> 
      <script type="text/javascript" src="js/bootstrap.js"></script>
          
</body>
</html>