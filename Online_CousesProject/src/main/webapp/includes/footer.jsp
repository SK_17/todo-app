<footer class="bg-primary text-white text-center text-lg-start">
  <!-- Grid container -->
  <div class="container p-4">
    <!--Grid row-->
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
       <ul class="list-unstyled mb-0">
          <li>
            <a href="#!" class="text-white">Techonology</a>
          </li>
          <li>
            <a href="#!" class="text-white">Get the app</a>
          </li>
          <li>
            <a href="#!" class="text-white">About us</a>
          </li>
          <li>
            <a href="#!" class="text-white">Contact us</a>
          </li>
        </ul>

     
      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
       

        <ul class="list-unstyled mb-0">
          <li>
            <a href="#!" class="text-white">Careers</a>
          </li>
          <li>
            <a href="#!" class="text-white">Blog</a>
          </li>
          <li>
            <a href="#!" class="text-white">Help and Support</a>
          </li>
          <li>
            <a href="#!" class="text-white">Investor</a>
          </li>
        </ul>
      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        

        <ul class="list-unstyled">
          <li>
            <a href="#!" class="text-white">Terms</a>
          </li>
          <li>
            <a href="#!" class="text-white">Privacy policy</a>
          </li>
          <li>
            <a href="#!" class="text-white">Cookies settings</a>
          </li>
          <li>
            <a href="#!" class="text-white">SiteMap</a>
          </li>
        </ul>
      </div>
      <!--Grid column-->
    </div>
    <!--Grid row-->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    � 2023 Copyright:
    <a class="text-white" href="https://mdbootstrap.com/">MDBootstrap.com</a>
  </div>
  <!-- Copyright -->
</footer>