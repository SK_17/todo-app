<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%> 
<%@page import="com.java.model.*"%>
    
<% 
      User auth =(User) request.getSession().getAttribute("auth");
      if(auth!=null){
	      request.setAttribute("person", auth);
       }
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
</head>
<body>
<div
    class="d-flex flex-column min-vh-100 justify-content-center align-items-center"
    id="template-bg-3">
    <div class="card mb-5 p-5  bg-dark bg-gradient text-white col-md-4">
        <div class="card-header text-center">
            <h3>Login</h3>
        </div>
        <div class="card-body mt-3">
            <form name="login" action="LoginServlet" method="post">
                <div class="input-group form-group mt-3">
                    <input type="text"
                        class="form-control text-center p-3"
                        placeholder="email" name="email" required>
                </div>
                <div class="input-group form-group mt-3">
                    <input type="password"
                        class="form-control text-center p-3"
                        placeholder="Password" name="password" required>
                </div>
                <div class="text-center">
                    <input type="submit" value="Login"
                        class="btn btn-primary mt-3 w-100 p-2"
                        name="login-btn">
                </div>
            </form>
                  
            </div>
        <div class="card-footer p-3">
            <div class="d-flex justify-content-center">
                <div class="text-primary">Don't have an account?<a href="registered.jsp">Registered Here.</a>
                    </div>
            </div>
        </div>
    </div>
</div>
<br>
<%@include file = "includes/footer.jsp" %>
<script type="text/javascript" src="js/bootstrap.js"></script>


</body>
</html>