<%@page import="com.java.connection.DbConnection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page language="java" import="java.util.*" %>
<%@page import="com.java.model.*"%>
 <%@page import="com.java.dao.*"%>   
<% 
      User auth =(User) request.getSession().getAttribute("auth");
      List<Courses>courses =null;
      if(auth!=null){
	      request.setAttribute("person", auth);
	     
	      CoursesDao coursesDao  = new CoursesDao(DbConnection.getConnection());
			courses = coursesDao.userCourses(auth.getId());
		}else{
			response.sendRedirect("login.jsp");
		}
          
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">


<link href="style/create.css" rel="stylesheet" type="text/css">

</head>
<body>
<%@include file ="includes\navbar.jsp" %>
<div class="container">
		<div class="container">
			  <div class="py-5 ">
			    <h2>Courses</h2>
  			  </div>
          </div> 
		<table class="table table-light">
			<thead>
				<tr>
					<th scope="col">Courses</th>
					<th scope="col">Start Date</th>
					<th scope="col">End date</th>
					
				</tr>
			</thead>
			<tbody>
			
			<%
			if(courses != null){
				for(Courses c:courses){%>
					<tr>
						<td><%=c.getCourses() %></td>
						<td><%=c.getStartdate()%></td>
						<td><%=c.getEnddate() %></td>
					</tr>
				<%}
			}
			%>
			
			</tbody>
		</table>
	</div>
	<%@include file ="includes\footer.jsp" %>
	<script type="text/javascript" src="js/bootstrap.js"></script>
</body>
</html>